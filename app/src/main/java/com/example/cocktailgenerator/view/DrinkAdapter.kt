package com.example.cocktailgenerator.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cocktailgenerator.databinding.ListCocktailBinding
import com.example.cocktailgenerator.model.models.Drink

class DrinkAdapter : RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {

    private lateinit var data: List<Drink>

    class DrinkViewHolder(private val binding: ListCocktailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun apply(data: Drink) = with(binding) {
            Log.d("apply", "apply: ${data.strDrinkThumb}")
            if (!data.strDrinkThumb.isNullOrEmpty()) {
                drinkListImg.loadImage(data.strDrinkThumb)
            }
            drinkListName.text = data.strDrink
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        val binding =
            ListCocktailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DrinkViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
        holder.itemView.setOnClickListener {
            Log.d("logd", "onBindViewHolder: $it")
            val img = item.strDrinkThumb
            val name = item.strDrink
            val ingredients = mutableListOf<String>(
                item.strIngredient1?.toString().orEmpty(),
                item.strIngredient2?.toString().orEmpty(),
                item.strIngredient3?.toString().orEmpty(),
                item.strIngredient4?.toString().orEmpty(),
                item.strIngredient5?.toString().orEmpty(),
                item.strIngredient6?.toString().orEmpty(),
                item.strIngredient7?.toString().orEmpty(),
                item.strIngredient8?.toString().orEmpty(),
                item.strIngredient9?.toString().orEmpty(),
                item.strIngredient10?.toString().orEmpty(),
                item.strIngredient11?.toString().orEmpty(),
                item.strIngredient12?.toString().orEmpty(),
                item.strIngredient13?.toString().orEmpty(),
                item.strIngredient14?.toString().orEmpty(),
                item.strIngredient15?.toString().orEmpty()
            ).toTypedArray()
            val instructions = item.strInstructions
            val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(img, name, ingredients, instructions)
            Navigation.findNavController(it).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyDrinks(drinks: List<Drink>) {
        data = drinks
    }

}

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}