package com.example.cocktailgenerator.model

import android.util.Log
import com.example.cocktailgenerator.model.models.Drink
import com.example.cocktailgenerator.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.lang.Exception

class Repo {

    val api by lazy { API.retrofitInstance }
    //
    suspend fun bundleDrinks() = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = mutableListOf<Drink>()
            val call1 = api.fetchDrinks()
            val call2 = api.fetchDrinks()
            val call3 = api.fetchDrinks()
            val call4 = api.fetchDrinks()
            val call5 = api.fetchDrinks()
            val calls = listOf(call1, call2, call3, call4, call5)
//            Log.d("callsList", "bundleDrinks: $calls")
            for (i in calls) {
                Log.d("calls", "bundleDrinks: ${i.body()!!.drinkList}")
                response.add(i.body()!!.drinkList[0])
            }
            Log.d("logd", "bundleDrinks: $response")
            if (call1.isSuccessful && call1.body()?.drinkList!!.isNotEmpty()) {
                Resource.Success(response)
            }
            else {
                Resource.Error("Something went wrong...")
            }
        }
        catch (e: Exception) {
            Log.d("e", "bundleDrinks: $e")
            Resource.Error(e.localizedMessage?.toString())
        }
    }

}