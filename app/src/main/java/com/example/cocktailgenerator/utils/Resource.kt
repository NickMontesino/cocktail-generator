package com.example.cocktailgenerator.utils

import com.example.cocktailgenerator.model.models.Drink

sealed class Resource(data: List<Drink>?, errorMsg: String?) {
    data class Success(val data: List<Drink>) : Resource(data, null)
    object Loading : Resource(null, null)
    data class Error(val errorMsg: String?) : Resource(null, errorMsg)
}
