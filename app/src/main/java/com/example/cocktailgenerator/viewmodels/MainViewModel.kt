package com.example.cocktailgenerator.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailgenerator.model.Repo
import com.example.cocktailgenerator.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val repo by lazy { Repo() }

    private val _drinks: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val drinks: LiveData<Resource> get() = _drinks

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _drinks.value = repo.bundleDrinks()
        }
    }

}