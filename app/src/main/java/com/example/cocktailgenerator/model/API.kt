package com.example.cocktailgenerator.model

import com.example.cocktailgenerator.model.models.Drink
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface API {

    data class DrinkResponse(
        @SerializedName("drinks")
        val drinkList: List<Drink>
    )

    @GET("random.php")
    suspend fun fetchDrinks(): Response<DrinkResponse>

    companion object {
        val retrofitInstance by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.thecocktaildb.com/api/json/v1/1/")
                .build()
                .create(API::class.java)
        }
    }

}