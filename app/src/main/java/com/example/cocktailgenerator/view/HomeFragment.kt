package com.example.cocktailgenerator.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailgenerator.databinding.FragmentHomeBinding
import com.example.cocktailgenerator.utils.Resource
import com.example.cocktailgenerator.viewmodels.MainViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!

    private val vm by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        getDrinksButton.setOnClickListener {
            initObservers()
        }
    }

    private fun initObservers() = with(vm) {

        drinks.observe(viewLifecycleOwner) { viewState ->
            Log.d("TAG", "initObservers: $viewState")
            when(viewState) {
                is Resource.Error -> {

                }
                is Resource.Loading -> {

                }
                is Resource.Success -> {
                    binding.drinkList.layoutManager = LinearLayoutManager(requireContext())
                    binding.drinkList.adapter = DrinkAdapter().apply {
                        Log.d("debug", "initObservers: ${viewState.data}")
                        applyDrinks(viewState.data)
                    }
                }
            }
        }

    }

}